import torch
import torch.nn as nn
import torch.nn.utils.prune as pr


def _get_bn_conv_couples(model):
    data = []
    for n, m in model.named_modules():
        if isinstance(m, nn.BatchNorm2d):
            if n[-1].isdigit() and n[-2] == '.':
                n = n[:-1] + str(int(n[-1]) - 1)
            n = n.replace('bn', 'conv')
            conv = model.state_dict()[n + '.weight']
            data.append([m, conv])
    return data


def _get_costs_and_values(data):
    costs = []
    values = []
    for d in data:
        values.append(d[0].weight.abs())
        costs.append(torch.ones(d[0].weight.size(0)) * (d[1].size(1) * d[1].size(2) * d[1].size(3)))
    values = torch.cat(values)
    costs = torch.cat(costs)
    values, index = torch.sort(values)
    costs = costs[index]
    return costs, values


def _get_index_by_dichotomy(costs, pruning_rate):
    def dichotomy(costs, target, index, max, min):
        result = torch.sum(costs[:index])
        if result == target:
            return index
        else:
            if result < target:
                if int((index + max) / 2) == index:
                    return index
                else:
                    return dichotomy(costs, target, int((index + max) / 2), max, index)
            else:
                if int((index + min) / 2) == index:
                    return index
                else:
                    return dichotomy(costs, target, int((index + min) / 2), index, min)

    target = int(torch.sum(costs) * pruning_rate)
    return dichotomy(costs, target, int(len(costs) / 2), len(costs), 0)


def _prune_model(model, threshold):
    convs = {}
    for n, m in model.named_modules():
        if isinstance(m, nn.BatchNorm2d):
            bn_mask = m.weight.abs() >= threshold
            pr.custom_from_mask(m, 'weight', bn_mask)
            pr.custom_from_mask(m, 'bias', bn_mask)
            if n[-1].isdigit() and n[-2] == '.':
                n = n[:-1] + str(int(n[-1]) - 1)
            n = n.replace('bn', 'conv')
            weights = model.state_dict()[n + '.weight']
            mask = bn_mask.view(len(bn_mask), 1, 1, 1).expand(weights.shape)
            print(mask)
            convs[n] = mask
    for n, m in model.named_modules():
        if n in convs:
            pr.custom_from_mask(m, 'weight', convs[n])


def prune(model, pruning_rate):
    data = _get_bn_conv_couples(model)
    costs, values = _get_costs_and_values(data)
    index = _get_index_by_dichotomy(costs, pruning_rate)
    threshold = values[index]
    _prune_model(model, threshold)


def remove(model):
    for m in model.modules():
        if hasattr(m, 'weight_orig'):
            pr.remove(m, 'weight')
        if hasattr(m, 'bias_orig'):
            pr.remove(m, 'bias')
