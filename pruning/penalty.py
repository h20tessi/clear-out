import torch


def batchnorms_smooth_l1_penalty(multiplier=1e-5):
    def penalty(model):
        value = 0
        for m in model.modules():
            if isinstance(m, torch.nn.BatchNorm2d):
                x = m.weight.data
                value += torch.pow(x[x.abs() <= 1], 2).sum() + x[x.abs() > 1].sum()
        return value * multiplier

    return penalty
