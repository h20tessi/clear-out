import argparse


def parse_arguments():
    parser = argparse.ArgumentParser(description='Train and prune network')

    # PATHS

    parser.add_argument('--results_path', type=str, default="./results",
                        help="Where to save results  (default: './results')")

    parser.add_argument('--checkpoint_path', type=str, default="./checkpoints",
                        help="Where to save models  (default: './checkpoints')")

    parser.add_argument('--dataset_path', type=str, default="./dataset",
                        help="Where to get the dataset (default: './dataset')")

    # GENERAL

    parser.add_argument('--model', type=str, default="hrnet48",
                        help="Type of model (default: 'hrnet48')")

    parser.add_argument('--dataset', type=str, default="cityscapes",
                        help="dataset to use  (default: 'cityscapes')")

    parser.add_argument('--optimizer', type=str, default="sgd",
                        help="Type of optimizer (default: 'sgd')")

    parser.add_argument('--scheduler', type=str, default="poly",
                        help="Type of scheduler (default: 'poly')")

    parser.add_argument('--poly_exp', type=float, default=2,
                        help='If using poly scheduler: polynomial exponent of scheduler (default: 2)')

    parser.add_argument('--lr', type=float, default=0.01,
                        help='Learning rate (default: 0.01)')

    parser.add_argument('--wd', default="5e-4", type=float,
                        help='Weight decay rate (default: 5e-4)')

    parser.add_argument('--batch_size', type=int, default=1,
                        help='Input batch size for training (default: 1)')

    parser.add_argument('--test_batch_size', type=int, default=1,
                        help='Input batch size for testing (default: 1)')

    parser.add_argument('--epochs', type=int, default=200,
                        help='Number of epochs to train (default: 200)')

    parser.add_argument("--distributed", action="store_true", default=False,
                        help="Distributes the model across available GPUs (default: False)")

    parser.add_argument("--debug", action="store_true", default=False,
                        help="Debug mode (default: False)")

    parser.add_argument('--criterion', type=str, default="rmi",
                        help="Type of criterion (default: 'rmi')")

    parser.add_argument('--metrics', type=str, nargs='+', default=['miou'],
                        help="List of metrics (default: ['miou'])")

    parser.add_argument('--device', type=str, default="cuda",
                        help="Device to use (default: 'cuda')")

    # CLASSIFICATION

    parser.add_argument('--input_feature_maps', type=int, default=64,
                        help='Input feature maps of classification ResNets (default: 64)')

    # SEMANTIC SEGMENTATION

    parser.add_argument("--pretrained", action="store_true", default=False,
                        help="Use pretrained models (default: False)")

    # PRUNING

    parser.add_argument('--pruning_iterations', type=int, default=1,
                        help="In how many pruning/fine-tuning steps divide pruning (default: 1)")

    parser.add_argument('--pruning_rate', type=float, default=0.,
                        help='Pruning rate (default: 0.)')

    parser.add_argument('--smooth_l1_penalty', type=float, default=0,
                        help="Multiplier of the smooth-L1 penalty on batchnorms weights (default: 0)")

    parser.add_argument('--fine_tuning_epochs', type=int, default=15,
                        help="How many fine-tuning epochs after each pruning step (default: 15)")

    parser.add_argument('--additional_final_epochs', type=int, default=0,
                        help="How many more fine-tuning epochs at the very end (default: 0)")

    return parser.parse_args()
