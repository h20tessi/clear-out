# Clear-Out

## Training

### Example

Train ResNet-20 on CIFAR-10, pruned at 50% in one shot.

`python3 launch_experiment.py --model resnet20 --dataset cifar10 --optimizer sgd --scheduler multistep --lr 0.1 --wd 5e-4 --batch_size 256 --test_batch_size 1000 --epochs 300 --criterion crossentropy --metrics top1 top5 --device cuda --input_feature_maps 64 --pruning_iterations 1 --pruning_rate 0.5 --smooth_l1_penalty 1e-4 --fine_tuning_epochs 50`


### Arguments

**--results_path**: Where to save results  (default: './results')

* Accepted values: any path string.

**--checkpoint_path**: Where to save models  (default: './checkpoints')

* Accepted values: any path string.

**--dataset_path**: Where to get the dataset (default: './dataset')

* Accepted values: any path string.

**--model**: Type of model (default: 'hrnet48')

* Accepted values: 'hrnet18', 'hrnet32', 'hrnet48', 'resnet18', 'resnet34', 'resnet50', 'resnet101', 'resnet152',
  'resnet20', 'resnet32', 'resnet44', 'resnet56', 'resnet110'.

**--dataset**: dataset to use  (default: 'cityscapes')

* Accepted values: 'cityscapes', 'cifar10', 'cifar100', 'imagenet'.

**--optimizer**: Type of optimizer (default: 'sgd')

* Accepted values: 'sgd', 'adam'.

**--scheduler**: Type of scheduler (default: 'poly')

* Accepted values: 'poly', 'multistep'.

**--poly_exp**: If using poly scheduler: polynomial exponent of scheduler (default: 2)')

* Accepted values: any float value.

**--lr**: Learning rate (default: 0.01)')

* Accepted values: any float value.

**--wd**: Weight decay rate (default: 5e-4)')

* Accepted values: any float value.

**--batch_size**: Input batch size for training (default: 1)')

* Accepted values: any int value.

**--test_batch_size**: Input batch size for testing (default: 1)')

* Accepted values: any int value.

**--epochs**: Number of epochs to train (default: 200)')

* Accepted values: any int value.

**--distributed**: Distributes the model across available GPUs (default: False)

* Using this arguments sets the value on True.

**--debug**: Debug mode (default: False)

* Using this arguments sets the value on True.

**--criterion**: Type of criterion (default: 'rmi')

* Accepted values: 'rmi', 'crossentropy'.

**--metrics**: List of metrics (default: ["miou"])

* Accepted values: 'miou', 'top1', 'top5'.

**--device**: Device to use (default: 'cuda')

* Accepted values: any device string, such as 'cpu' or 'cuda'

**--input_feature_maps**: Input feature maps of classification ResNets (default: 64)')

* Accepted values: any int value.

**--pretrained**: Use pretrained models (default: False)

* Using this arguments sets the value on True.

**--pruning_iterations**: In how many pruning/fine-tuning steps divide pruning (default: 1)

* Accepted values: any int value.

**--pruning_rate**: Pruning rate (default: 0.)')

* Accepted values: any float value between 0 and 1.

**--smooth_l1_penalty**: Multiplier of the smooth-L1 penalty on batchnorms weights (default: 0)

* Accepted values: any float value.

**--fine_tuning_epochs**: How many fine-tuning epochs after each pruning step (default: 15)

* Accepted values: any int value.

**--additional_final_epochs**: How many more fine-tuning epochs at the very end (default: 0)

* Accepted values: any int value.

## Clear-Out

### Example

Clear out a ResNet-20 network trained on CIFAR-10

`python3 clearout.py --network_path ./path/to/network.sd --model resnet20 --dataset_path ./path/to/dataset/ --dataset cifar10 --test_batch_size 1000 --metrics top1 top5 --device cuda --input_feature_maps 64 --test --clearout_dataset_samples 100`

### Arguments

**--model**: Type of model (default: 'resnet20')

* Accepted values: 'hrnet18', 'hrnet32', 'hrnet48', 'resnet18', 'resnet34', 'resnet50', 'resnet101', 'resnet152',
  'resnet20', 'resnet32', 'resnet44', 'resnet56', 'resnet110'.

**--network_path**: Path to network to clear  (default: '')

* Accepted values: any path string.

**--dataset_path**: Where to get the dataset (default: './dataset')

* Accepted values: any path string.

**--dataset**: dataset to use  (default: 'cityscapes')

* Accepted values: 'cityscapes', 'cifar10', 'cifar100', 'imagenet'.

**--batch_size**: Input batch size for training (default: 1)

* Accepted values: any int value.

**--test_batch_size**: Input batch size for testing (default: 1)

* Accepted values: any int value.

**--clearout_dataset_samples**: Number of elements of the dataset to use to compute gradient for clearout (default: 100)

* Accepted values: any int value.

--**clearout_output_samples**: Number of randomly chosen samples of the output to backward per channel (default: -1)

* Accepted values: any int value.

**--input_feature_maps**: Input feature maps of classification ResNets (default: 64)

* Accepted values: any int value.

**--metrics**: List of metrics (default: ['miou'])

* Accepted values: 'miou', 'top1', 'top5'.

**--device**: Device to use (default: 'cuda')

* Accepted values: any device string, such as 'cpu' or 'cuda'


**--test**: Test before and after clearing (default: False)

Using this arguments sets the value on True.
