from utils import args, checkpoint, optimizer, scheduler, train, criterion, datasets, metrics, models
from pruning.methods import prune, remove
from pruning.penalty import batchnorms_smooth_l1_penalty
import numpy as np
import torch


def main():
    arguments = args.parse_arguments()

    model = models.get_model(arguments).to(arguments.device)
    optim = optimizer.get_optimizer(arguments, model)
    sched = scheduler.get_scheduler(arguments, optim)
    check = checkpoint.Checkpoint(model, optim, sched, arguments.device, arguments.distributed,
                                  arguments.checkpoint_path, 'initial_training')

    dataset = datasets.get_dataset(arguments)
    met = [metrics.get_metric(n) for n in arguments.metrics]
    crit = criterion.get_criterion(arguments.criterion)

    penalty = None
    if arguments.smooth_l1_penalty != 0:
        penalty = batchnorms_smooth_l1_penalty(arguments.smooth_l1_penalty)

    train.train_model(name='Training', checkpoint=check, dataset=dataset,
                      epochs=arguments.epochs, criterion=crit, metrics=met,
                      output_path=arguments.results_path, debug=arguments.debug,
                      device=arguments.device,
                      penalty=penalty)

    if arguments.pruning_rate != 0:
        rates = np.linspace(0, arguments.pruning_rate, arguments.pruning_iterations + 1)[1:]

        for i, r in enumerate(rates):
            name = f'Pruning iteration {i + 1}/{len(rates)}, rate={round(r, 2)}'
            print(f'\n\n{name}')
            prune(check.model, r)
            check.name = f'pruning_rate_{arguments.pruning_rate}_step({i + 1}_{len(rates)})'
            train.train_model(name=name, checkpoint=check, dataset=dataset,
                              epochs=arguments.fine_tuning_epochs if i + 1 != len(rates)
                              else arguments.fine_tuning_epochs + arguments.additional_final_epochs,
                              criterion=crit, metrics=met,
                              output_path=arguments.results_path, debug=arguments.debug,
                              device=arguments.device,
                              penalty=penalty if i + 1 != len(rates) else None,
                              freeze_lr=True)
            remove(check.model)
        with open(arguments.checkpoint_path + f'/pruned_model_{arguments.pruning_rate}.sd', 'wb') as f:
            torch.save(check.model.cpu().state_dict(), f)


if __name__ == '__main__':
    main()
