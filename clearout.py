import torch
import argparse
import sys
from utils import metrics, models, datasets


def parse_arguments():
    parser = argparse.ArgumentParser(description='Clear Out')

    parser.add_argument('--model', type=str, default="resnet20",
                        help="Type of model (default: 'resnet20')")

    parser.add_argument('--network_path', type=str, default="",
                        help="Path to network to clear  (default: '')")

    parser.add_argument('--dataset_path', type=str, default="./dataset",
                        help="Where to get the dataset (default: './dataset')")

    parser.add_argument('--dataset', type=str, default="cityscapes",
                        help="dataset to use  (default: 'cityscapes')")

    parser.add_argument('--batch_size', type=int, default=1,
                        help='Input batch size for training (default: 1)')

    parser.add_argument('--test_batch_size', type=int, default=1,
                        help='Input batch size for testing (default: 1)')

    parser.add_argument('--clearout_dataset_samples', type=int, default=100,
                        help='Number of elements of the dataset to use to compute gradient for clearout (default: 100)')

    parser.add_argument('--clearout_output_samples', type=int, default=-1,
                        help='Number of randomly chosen samples of the output to backward per channel (default: -1)')

    parser.add_argument('--input_feature_maps', type=int, default=64,
                        help='Input feature maps of classification ResNets (default: 64)')

    parser.add_argument('--metrics', type=str, nargs='+', default=['miou'],
                        help="List of metrics (default: ['miou'])")

    parser.add_argument('--device', type=str, default="cuda",
                        help="Device to use (default: 'cuda')")

    parser.add_argument("--test", action="store_true", default=False,
                        help="Test before and after clearing (default: False)")

    parser.add_argument("--distributed", action="store_true", default=False,
                        help="Distributes the model across available GPUs (default: False)")

    return parser.parse_args()


def test(network, loader, device, metrics, batch_size):
    network.zero_grad()
    network.eval()
    results = [0 for _ in range(len(metrics))]
    with torch.no_grad():
        for batch_idx, (input, target) in enumerate(loader):
            sys.stdout.write(f'\rTest: ({batch_idx + 1}/{len(loader)})')
            input, target = input.to(device), target.to(device)
            output = network(input)
            _, predicted = output.max(1)
            for k, m in enumerate(metrics):
                results[k] += m(output, target)

        message = f'\rTest -> '
        for k, m in enumerate(metrics):
            message += f'{m.name} = {round(float(results[k]) / (len(loader) * batch_size), 3)}\t'
        print('\n' + message)


if __name__ == '__main__':
    arguments = parse_arguments()
    dataset = datasets.get_dataset(arguments)
    train_loader = dataset['train']
    test_loader = dataset['test']
    net = models.get_model(arguments)
    if arguments.distributed:
        net = torch.nn.DataParallel(net)
    net.load_state_dict(torch.load(arguments.network_path))
    net = net.to(arguments.device)
    met = [metrics.get_metric(n) for n in arguments.metrics]

    if arguments.test:
        print('Before clearout')
        test(net, test_loader, arguments.device, met, arguments.test_batch_size)

    grad = {}
    for i, (data, _) in enumerate(train_loader):
        if i >= arguments.clearout_dataset_samples:
            break
        data = data.to(arguments.device)
        sys.stdout.write(f'\rGradient computation: ({i + 1}/{len(train_loader)})')
        out = net(data)[0]
        for channel in out:
            if len(channel.size()) == 0:
                net.zero_grad()
                channel.backward(retain_graph=True)
                for n, p in net.named_parameters():
                    if n not in grad:
                        grad[n] = p.grad.data.abs()
                    else:
                        grad[n] += p.grad.data.abs()
            else:
                if arguments.clearout_output_samples != -1:
                    n = len(channel.flatten())
                    channel = channel.flatten()[torch.multinomial(torch.linspace(0, n - 1, n),
                                                                  arguments.clearout_output_samples)]
                net.zero_grad()
                for c in channel:
                    c.backward(retain_graph=True)
                    for n, p in net.named_parameters():
                        if n not in grad:
                            grad[n] = p.grad.data.abs()
                        else:
                            grad[n] += p.grad.data.abs()

    print()

    print('Total params : ', len(torch.cat([i.flatten() for i in net.parameters()])))
    print('After pruning : ', torch.sum(torch.cat([i.flatten() for i in net.parameters()]) != 0).item())

    count = 0
    for n, p in net.named_parameters():
        count += torch.sum(grad[n] != 0)
        p.data *= (grad[n] != 0).float()

    print('After clearout : ', torch.sum(torch.cat([i.flatten() for i in net.parameters()]) != 0).item())
    if arguments.test:
        test(net, test_loader, arguments.device, met, arguments.test_batch_size)
    torch.save(net.cpu().state_dict(), arguments.network_path[:-3] + '_cleared' + arguments.network_path[-3:])
